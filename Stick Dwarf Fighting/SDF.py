import pygame
import os.path

pygame.init()
class Display:
    width = 800
    height = 600

    # Just some color prefixes - not used, except cyan
    black = (0, 0, 0)
    white = (255, 255, 255)
    cyan = (0, 255, 255)
    color = cyan
    #
class Game:
    location = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
    display = pygame.display.set_mode((Display.width,Display.height))
    pygame.display.set_caption("Stick Dwarf Fighting")
    icon = pygame.image.load("{0}\\{1}".format(location, "Textures\\gimli_ico.png"))
    pygame.display.set_icon(icon)
    clock = pygame.time.Clock()
    pygame.display.flip()
    exit = False
    player = True
    #
class Player:
    width = 10
    width_change = 0
    jump_force = 20
    height_change = -jump_force
    jump = False
    characters = ["char3.png", "tux.png"]
    bow = ["bow0.png", "bow2.png", "bow3.png", "bow4.png", "bow5.png"]
    weapons = [bow]
    loc = 0
    weapon = 0
    weapon_type = 0
    arrows = 5
    firing_release = False
    firing = False
    arrow = 0
    arrow_stance = 0
    img = pygame.image.load("{0}\\{1}\\{2}".format(Game.location, "Textures\\Characters", characters[loc]))
    height = Display.height - int(img.get_rect().size[0])
    #
class Self:
    firing = 0
    def reload():
        if Game.player == True:
            Game.display.blit(Player.img, (Player.width, Player.height))
            if Player.weapon != 0:
                Game.display.blit(Player.weapon, (Player.width, Player.height-100))
                if Player.firing == True or Player.firing == "Ready":
                    Game.display.blit(Player.arrow, (Player.width + 208 - Player.arrow_stance, Player.height - 4))

    def image(self):
        img = pygame.image.load("{0}\\{1}\\{2}".format(Game.location, "Textures\\Characters", Player.characters[self]))
        return img

    def weapon_select(self, nr = 0):
        img = pygame.image.load("{0}\\{1}\\{2}".format(Game.location, "Textures\\Weapons", Player.weapons[self][nr]))
        return img

    def arrow():
        img = pygame.image.load("{0}\\{1}".format(Game.location, "Textures\\Ammunition\\arrow.png"))
        return img
    #
class Monster:
    img = pygame.image.load("{0}\\{1}".format(Game.location, "Textures\\Characters\\emblik.png"))
    def reload():
        Game.display.blit(Monster.img, (500, 300))


class Arrow:
    x = Player.width + 208 - Player.arrow_stance
    y = Player.height - 4
    lendab = False

class Arrow2:
    def flying():
        if Player.firing_release == "Flying":
            Arrow.x = Player.width + 208 - Player.arrow_stance
            Arrow.y = Player.height - 4
            Player.firing_release = False
            Arrow.lendab = True
            Game.display.blit(Player.arrow, (Arrow.x, Arrow.y))
        if Player.firing_release == False and Arrow.lendab == True:
            Arrow.x += 15
            Game.display.blit(Player.arrow, (Arrow.x, Arrow.y))
        if Arrow.x > Display.width:
            Arrow.lendab = False


def game_loop():
    while not Game.exit:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                Game.exit = True
            if Game.player == True:  # Alive
                if event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_RIGHT:
                        Player.width_change = 4
                    elif event.key == pygame.K_LEFT:
                        Player.width_change = -4
                    if event.key == pygame.K_UP:
                        Player.jump = True
                    if event.key == pygame.K_f and Player.jump == False:
                        if Player.loc == 0:
                            Player.loc = 1
                            Player.width += 60
                        else:
                            Player.loc = 0
                            Player.width -= 60
                        Player.img = Self.image(Player.loc)
                        Player.height = Display.height - int(Player.img.get_rect().size[0])
                    if event.key == pygame.K_1:
                        if Player.weapon_type == 1:
                            Player.weapon_type = 0
                            Player.weapon = 0
                        else:
                            Player.weapon_type = 1
                            Player.weapon = Self.weapon_select(0)
                    if event.key == pygame.K_SPACE and Player.arrows > 0 and Player.weapon_type == 1:
                        Player.firing = True
                if event.type == pygame.KEYUP:
                    if event.key == pygame.K_LEFT or event.key == pygame.K_RIGHT:
                        Player.width_change = 0
                    if event.key == pygame.K_SPACE:
                        Player.firing_release = True
        if Player.jump == True and Player.height_change < (Player.jump_force + 1):
            Player.height += Player.height_change
            Player.height_change += 1
        elif Player.height_change == (Player.jump_force + 1):
            Player.jump = False
            Player.height_change = -Player.jump_force
        if Player.width < 10 and Player.width_change < 0:
            Player.width_change = 0
        if Player.width > Display.width - 60 and Player.width_change > 0:
            Player.width_change = 0
        if Player.weapon_type == 1 and Player.firing is True:
            if Self.firing == 0:
                Player.arrow = Self.arrow()
            Self.firing += 5
            if Self.firing == 20:
                Player.weapon = Self.weapon_select(0, 1)
                Player.arrow_stance += 10
            if Self.firing == 40:
                Player.weapon = Self.weapon_select(0, 2)
                Player.arrow_stance += 10
            if Self.firing == 60:
                Player.weapon = Self.weapon_select(0, 3)
                Player.arrow_stance += 10
            if Self.firing == 80:
                Player.weapon = Self.weapon_select(0, 4)
                Player.arrow_stance += 10
                Player.firing = "Ready"
                Self.firing = 0
                Player.arrows -= 1
        if Player.firing == "Ready" and Player.firing_release is True:
            Self.firing += 5
            if Self.firing == 10:
                Player.weapon = Self.weapon_select(0, 3)
                Player.arrow_stance -= 10
            if Self.firing == 20:
                Player.weapon = Self.weapon_select(0, 2)
                Player.arrow_stance -= 10
            if Self.firing == 30:
                Player.weapon = Self.weapon_select(0, 1)
                Player.arrow_stance -= 10
            if Self.firing == 35:
                Player.weapon = Self.weapon_select(0, 0)
                Player.arrow_stance -= 10
                Player.firing = False
                Player.firing_release = "Flying"
                Self.firing = 0
                Player.arrow_stance = 0

        Player.width += Player.width_change
        Game.display.fill(Display.color)
        Arrow2.flying()
        Monster.reload()
        Self.reload()
        pygame.display.update()
        Game.clock.tick(60)


game_loop()
pygame.quit()
quit()