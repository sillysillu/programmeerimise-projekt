# Written by Silver Kirotar.
from tkinter import *
from tkinter import ttk
import time
import os.path

raam = Tk()
raam.title("Vibu laskmine")
tahvel = Canvas(raam, width=800, height=600, background="cyan")
tahvel.grid()
raam.resizable(0, 0)

#print(os.path.abspath(__file__)) #for debugging

class global_variables():
    game_location = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
    ammonition_type2 = 0
    ammonition_type = 0
    ammonition = False
    weapon_type2 = 0
    weapon_type = 0
    weapon = False
    x = 404
    y = 404
    z = 0
    img_char = PhotoImage(file=game_location + "\\Textures\\Characters\\char2.png")
    #img_char = PhotoImage(file=game_location + "\\.png")
    char = tahvel.create_image(x, y, image=img_char)
    bow_stance_0 = 0
    bow_stance_1 = 0
    bow_stance_2 = 0
    ammonition_sprite = 0
    moving_speed = 8
    #vana_x = x
    #vana_y = y
    
class bow_class():
    def failsafe(event):
        asd = 1
    def load_regular_bow(): # Panna muutuja piltide valimiseks, või teha mitmeosalised pildid nagu MC's?
        loc = global_variables.game_location
        print(loc)
        bow_stance_0 = PhotoImage(file=(loc + "\\Textures\\Weapons\\Range\\bow3.png"))
        bow_stance_1 = PhotoImage(file=(loc + "\\Textures\\Weapons\\Range\\bow2.png"))
        bow_stance_2 = PhotoImage(file=(loc + "\\Textures\\Weapons\\Range\\bow0.png"))
        return bow_stance_0, bow_stance_1, bow_stance_2
    def load_arrow(): # Panna juurde muutuja, mis määrab mis pilt laadida? Ei tea kas tasub.. võib erroreid tekitada hiljem.
        g = global_variables
        if g.ammonition_type2 == 0:
            g.ammonition_sprite = PhotoImage(file=(g.game_location + "\\Textures\\Ammonition\\arrow.png"))
    def reload(event):
        bow_class.load_arrow()
        tahvel.itemconfigure(global_variables.weapon_type, image=global_variables.bow_stance_0)
        def klõps(event):
            tahvel.delete(global_variables.ammonition_type)
        def tühik(event):
            figure = 0
            x = 30
            g = global_variables
            bow_stance = [g.bow_stance_1, g.bow_stance_2]
            def animation(figure):
                if figure < 2:
                    tahvel.move(global_variables.ammonition_type, 10, 0)
                    tahvel.itemconfigure(global_variables.weapon_type, image=bow_stance[figure])
                    #global_variables.vana_x += 3
                else:
                    for i in range(70):
                        tahvel.move(global_variables.ammonition_type, 4, 0)
                        #global_variables.vana_x += 3
                        tahvel.update_idletasks()
                tahvel.update_idletasks()
            for i in range(3):
                if global_variables.ammonition_type2 == 1:
                    if i > 1:
                        x = 3
                    tahvel.after(12, animation(i))
            #vana_ammonition_type = tahvel.create_image(g.vana_x+76, g.vana_y, image=g.ammonition_sprite)
            #tahvel.delete(g.ammonition_type)
            #tahvel.tag_bind(vana_ammonition_type, '<1>', klõps)
            tahvel.tag_bind(g.ammonition_type, '<1>', klõps)
            global_variables.ammonition_type2 = 0
        g = global_variables
        if g.ammonition_type2 == 0:
            g.ammonition_type = tahvel.create_image(g.x+156, g.y-110, image=g.ammonition_sprite)
            g.ammonition_type2 = 1
            raam.bind_all("<space>", tühik)
        
class movement():
    def move_weapon():
        g = global_variables
        if g.ammonition_type2 == 1 and g.weapon == True:
            tahvel.coords(g.ammonition_type, g.x+156, g.y-110)
            tahvel.coords(g.weapon_type, g.x, g.y-100)
        elif g.weapon == True:
            tahvel.coords(g.weapon_type, g.x, g.y-100)
            
    def move_up(event):
        g = global_variables
        g.y -= g.moving_speed
        tahvel.coords(g.char, g.x, g.y)
        movement.move_weapon()
        
    def move_down(event):
        g = global_variables
        g.y += g.moving_speed
        tahvel.coords(g.char, g.x, g.y)
        movement.move_weapon()
        
    def move_left(event):
        g = global_variables
        g.x -= g.moving_speed
        tahvel.coords(g.char, g.x, g.y)
        movement.move_weapon()
        
    def move_right(event):
        g = global_variables
        g.x += g.moving_speed
        tahvel.coords(g.char, g.x, g.y)
        movement.move_weapon()
def select_second_weapon(event):
    loc = global_variables.game_location
    mook = PhotoImage(file=(loc + "\\sga.png"))
    tahvel.create_image(global_variables.x, global_variables.y-110, image=mook)
    
def select_first_weapon(event):
    g = global_variables
    g.bow_stance_0, g.bow_stance_1, g.bow_stance_2 = bow_class.load_regular_bow()
    if g.weapon_type2 != "vibu":
        g.weapon_type = tahvel.create_image(global_variables.x, global_variables.y-110, image=g.bow_stance_2)
        g.weapon_type2 = "vibu"
        print(str(g.x) +","+ str(g.y) )
        tahvel.update_idletasks()
        g.weapon = True
        raam.bind_all("<r>", bow_class.reload)
        raam.bind_all("<R>", bow_class.reload)
    else:
        raam.bind_all("<r>", bow_class.failsafe)
        raam.bind_all("<R>", bow_class.failsafe)
        raam.bind_all("<space>", bow_class.failsafe)
        tahvel.delete(g.weapon_type)
        tahvel.delete(g.ammonition_type)
        g.weapon = False
        g.ammonition_type2 = 0
        g.weapon_type2 = 0
        #return bow

#arrow = tahvel.create_image(x+3, y, image=nool)
#raam.bind_all("<space>", tühik)

class inventory():
    inventory1 = select_first_weapon
    inventory2 = select_second_weapon
raam.bind_all(2, inventory.inventory2)    
raam.bind_all(1, inventory.inventory1)
raam.bind_all("<Up>",    movement.move_up)
raam.bind_all("<W>",    movement.move_up)
raam.bind_all("<w>",    movement.move_up)
raam.bind_all("<Down>",  movement.move_down)
raam.bind_all("<S>",    movement.move_down)
raam.bind_all("<s>",    movement.move_down)
raam.bind_all("<Left>",  movement.move_left)
raam.bind_all("<A>",    movement.move_left)
raam.bind_all("<a>",    movement.move_left)
raam.bind_all("<Right>", movement.move_right)
raam.bind_all("<D>",    movement.move_right)
raam.bind_all("<d>",    movement.move_right)
raam.mainloop()